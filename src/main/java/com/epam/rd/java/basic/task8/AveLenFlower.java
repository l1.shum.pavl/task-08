package com.epam.rd.java.basic.task8;

public class AveLenFlower {
    private String measure;

    private String content;

    public AveLenFlower(String measure, String content) {
        this.measure = measure;
        this.content = content;
    }

    public String getMeasure() {
        return measure;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "measure='" + measure + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}

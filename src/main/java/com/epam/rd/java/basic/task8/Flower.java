package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String origin;
    private String soil;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;

    public Flower(String name, String origin, String soil, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.origin = origin;
        this.soil = soil;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public String getOrigin() {
        return origin;
    }

    public String getName() {
        return name;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public String getSoil() {
        return soil;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                ", origin='" + origin + '\'' +
                ", name='" + name + '\'' +
                ", visualParameters=" + visualParameters +
                ", soil='" + soil + '\'' +
                '}';
    }


}
package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    public void setFlower(Flower flower) {
        flowers.add(flower);
    }

    private List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlower ()
    {
        return flowers;
    }
}

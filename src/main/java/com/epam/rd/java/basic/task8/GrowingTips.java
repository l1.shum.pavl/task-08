package com.epam.rd.java.basic.task8;

public class GrowingTips {
    private Tempreture tempreture;

    private Watering watering;

    private Lighting lighting;

    public GrowingTips(Tempreture tempreture, Watering watering, Lighting lighting) {
        this.tempreture = tempreture;
        this.watering = watering;
        this.lighting = lighting;
    }

    public Tempreture getTempreture() {
        return tempreture;
    }

    public Watering getWatering() {
        return watering;
    }

    public Lighting getLighting() {
        return lighting;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", watering=" + watering +
                ", lighting=" + lighting +
                '}';
    }
}

package com.epam.rd.java.basic.task8;

public class Lighting {
    private String lightRequiring;

    public Lighting(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    @Override
    public String toString() {
        return "Lighting{" +
                "lightRequiring='" + lightRequiring + '\'' +
                '}';
    }
}

package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse();
		// sort (case 1)
		domController.flowers.getFlower().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower a, Flower b) {
				return a.getName().compareTo(b.getName());
			}
		});

		domController.flowers.getFlower().forEach(System.out::println);
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.toFile(domController.flowers,outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.startParse();
		// sort  (case 2)
		saxController.fls.getFlower().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower a, Flower b) {
				return a.getGrowingTips().getWatering().getContent().compareTo(
						b.getGrowingTips().getWatering().getContent()
				);
			}
		});

		saxController.fls.getFlower().forEach(System.out::println);
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.toFile(domController.flowers,outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.staxParse();
		// sort  (case 3)
		staxController.fls.getFlower().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower a, Flower b) {

				return b.getVisualParameters().getAveLenFlower().getContent().compareTo(
						a.getVisualParameters().getAveLenFlower().getContent()
				);
			}
		});

		staxController.fls.getFlower().forEach(System.out::println);
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		domController.toFile(domController.flowers,outputXmlFile);
	}

}

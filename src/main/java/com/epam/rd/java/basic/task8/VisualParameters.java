package com.epam.rd.java.basic.task8;

public class VisualParameters {
    private AveLenFlower aveLenFlower;

    private String stemColour;

    private String leafColour;

    public VisualParameters(AveLenFlower aveLenFlower, String stemColour, String leafColour) {
        this.aveLenFlower = aveLenFlower;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "aveLenFlower=" + aveLenFlower +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                '}';
    }
}

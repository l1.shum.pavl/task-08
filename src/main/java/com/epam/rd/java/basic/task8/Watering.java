package com.epam.rd.java.basic.task8;

public class Watering {
    private String measure;

    private String content;

    public Watering(String measure, String content) {
        this.measure = measure;
        this.content = content;
    }

    public String getMeasure() {
        return measure;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "measure='" + measure + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}

package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	public Flowers flowers = new Flowers();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse () throws ParserConfigurationException, IOException, SAXException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document =  builder.parse(new File(xmlFileName));


		NodeList flowerElements = document.getDocumentElement().getElementsByTagName("flower");
		for (int i = 0; i < flowerElements.getLength(); i++) {
			Element fl = (Element) flowerElements.item(i);
			flowers.setFlower(buildFlower(fl));
		}
//		flowers.getFlower().forEach(System.out::println);
	}

	public void toFile(Flowers flowers, String fileName) throws ParserConfigurationException {
		List<Flower> flowerList = flowers.getFlower();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.newDocument();
		String root = "flowers";
		Element rootElement = document.createElement(root);

		rootElement.setAttribute("xmlns", "http://www.nure.ua");
		rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");

		document.appendChild(rootElement);

		for(int i=0; i<flowerList.size();i++){
			Flower its = flowerList.get(i);
			Element flower = document.createElement("flower");
			rootElement.appendChild(flower);

			Element name = document.createElement("name");
			name.appendChild(document.createTextNode(its.getName()));
			flower.appendChild(name);

			Element soil = document.createElement("soil");
			soil.appendChild(document.createTextNode(its.getSoil()));
			flower.appendChild(soil);

			Element origin = document.createElement("origin");
			origin.appendChild(document.createTextNode(its.getOrigin()));
			flower.appendChild(origin);

			Element visualParameters = document.createElement("visualParameters");

			Element stemCol = document.createElement("stemColour");
			stemCol.appendChild(document.createTextNode(its.getVisualParameters().getStemColour()));
			visualParameters.appendChild(stemCol);

			Element leafCol = document.createElement("leafColour");
			leafCol.appendChild(document.createTextNode(its.getVisualParameters().getLeafColour()));
			visualParameters.appendChild(leafCol);

			Element aveLenFlower = document.createElement("aveLenFlower");
			aveLenFlower.appendChild(document.createTextNode(its.getVisualParameters().getAveLenFlower().getContent()));
			aveLenFlower.setAttribute("measure" , its.getVisualParameters().getAveLenFlower().getMeasure());

			visualParameters.appendChild(aveLenFlower);

			flower.appendChild(visualParameters);


			Element growingTips = document.createElement("growingTips");

			Element tempreture = document.createElement("tempreture");
			tempreture.appendChild(document.createTextNode(its.getGrowingTips().getTempreture().getContent()));
			tempreture.setAttribute("measure", its.getGrowingTips().getTempreture().getMeasure());
			growingTips.appendChild(tempreture);

			Element lighting = document.createElement("lighting");
			lighting.setAttribute("lightRequiring", its.getGrowingTips().getLighting().getLightRequiring());
			growingTips.appendChild(lighting);

			Element watering = document.createElement("watering");
			watering.appendChild(document.createTextNode(its.getGrowingTips().getWatering().getContent()));
			watering.setAttribute("measure", its.getGrowingTips().getWatering().getMeasure());
			growingTips.appendChild(watering);

			flower.appendChild(growingTips);

			Element mult = document.createElement("multiplying");
			mult.appendChild(document.createTextNode(its.getMultiplying()));
			flower.appendChild(mult);


		}



		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new FileWriter(fileName));
			transformer.transform(source, result);
	} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

		private Flower buildFlower (Element fl ){
		return new Flower(fl.getElementsByTagName("name").item(0).getTextContent(),
				fl.getElementsByTagName("origin").item(0).getTextContent(),
				fl.getElementsByTagName("soil").item(0).getTextContent(),
				buildVisulaParameters((Element) fl.getElementsByTagName("visualParameters").item(0)),
				buildGrovingTips((Element) fl.getElementsByTagName("growingTips").item(0)),
				fl.getElementsByTagName("multiplying").item(0).getTextContent()
		);
	}

	private VisualParameters buildVisulaParameters (Element fl ){
		return new VisualParameters(
				buildAvelenFlower((Element) fl.getElementsByTagName("aveLenFlower").item(0)),
				fl.getElementsByTagName("stemColour").item(0).getTextContent(),
				fl.getElementsByTagName("leafColour").item(0).getTextContent()
		);
	}

	private GrowingTips buildGrovingTips (Element fl ){
		return new GrowingTips(
				buildTemperature((Element) fl.getElementsByTagName("tempreture").item(0)),
				buildWatering((Element) fl.getElementsByTagName("watering").item(0)),
				buildLighting((Element) fl.getElementsByTagName("lighting").item(0))
		);
	}

	private Watering buildWatering (Element fl ){
		return new Watering(fl.getAttribute("measure"),
				fl.getTextContent());
	}

	private Lighting buildLighting (Element fl ){
		return new Lighting(fl.getAttribute("lightRequiring"));
	}

	private Tempreture buildTemperature (Element fl ){
		return new Tempreture(fl.getAttribute("measure"),
				fl.getTextContent());
	}

	private AveLenFlower buildAvelenFlower (Element fl ){
		return new AveLenFlower(fl.getAttribute("measure"),
				fl.getTextContent());
	}

}

package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	XMLHandler handler = new XMLHandler();
	public Flowers fls = handler.fls;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void startParse() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		parser.parse(new File(xmlFileName), handler);

	}

	private static class XMLHandler extends DefaultHandler{

		String thisElement = "";
		String elementText = "";
		String elementAtr = "";

		Tempreture tempreture;
		Watering watering;
		Lighting lighting;
		GrowingTips growingTips;
		AveLenFlower aveLenFlower;
		VisualParameters visualParameters;
		String vps;
		String vpl;
		Flower flower;
		String fn;
		String fo;
		String fs;
		String fmp;

		public Flowers fls = new Flowers();


		@Override
		public void startDocument() throws SAXException {
			System.out.println("sax start");
		}

		@Override
		public void endDocument() throws SAXException {
			System.out.println("sax stop");
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals("watering")){
				thisElement = qName;
				elementAtr = attributes.getValue("measure");
			}
			if (qName.equals("lighting")){
				thisElement = qName;
				elementAtr = attributes.getValue("lightRequiring");
			}
			if (qName.equals("tempreture")){
				thisElement = qName;
				elementAtr = attributes.getValue("measure");
			}

			if (qName.equals("aveLenFlower")){
				thisElement = qName;
				elementAtr = attributes.getValue("measure");
			}

			if (qName.equals("stemColour")){
				thisElement = qName;
			}

			if (qName.equals("leafColour")){
				thisElement = qName;
			}

			if (qName.equals("name")){
				thisElement = qName;
			}

			if (qName.equals("soil")){
				thisElement = qName;
			}

			if (qName.equals("origin")){
				thisElement = qName;
			}

			if (qName.equals("multiplying")){
				thisElement = qName;
			}


		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equals("tempreture")){
				tempreture = new Tempreture(elementAtr, elementText);
			}
			if (qName.equals("watering")){
				watering = new Watering(elementAtr, elementText);
			}
			if (qName.equals("lighting")){
				lighting = new Lighting(elementAtr);
			}

			if (qName.equals("growingTips")){
				growingTips = new GrowingTips(tempreture, watering, lighting);

			}

			if (qName.equals("aveLenFlower")){
				aveLenFlower = new AveLenFlower(elementAtr, elementText);
			}

			if (qName.equals("visualParameters")){
				visualParameters = new VisualParameters(aveLenFlower, vps, vpl);

			}

			if (qName.equals("flower")){
				flower = new Flower(fn, fo, fs, visualParameters, growingTips, fmp);

				fls.setFlower(flower);
			}

			if (qName.equals("flowers")){
//				this.fls.getFlower().forEach(System.out::println);

			}

			thisElement = "";
			elementText = "";
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {

			if (thisElement.equals("watering")){
				elementText = new String(ch, start, length);
			}

			if (thisElement.equals("tempreture")){
				elementText = new String(ch, start, length);
			}

			if (thisElement.equals("aveLenFlower")){
				elementText = new String(ch, start, length);
			}

			if (thisElement.equals("leafColour")){
				vpl = new String(ch, start, length);
			}

			if (thisElement.equals("stemColour")){
				vps = new String(ch, start, length);
			}

			if (thisElement.equals("name")){
				fn = new String(ch, start, length);
			}

			if (thisElement.equals("origin")){
				fo = new String(ch, start, length);
			}

			if (thisElement.equals("soil")){
				fs = new String(ch, start, length);
			}

			if (thisElement.equals("multiplying")){
				fmp = new String(ch, start, length);
			}
		}
	}

}
package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

	public Flowers fls = new Flowers();


	public STAXController(String xmlFileName) throws XMLStreamException, FileNotFoundException {
		this.xmlFileName = xmlFileName;
	}


	public void staxParse() throws XMLStreamException, FileNotFoundException {
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
		Tempreture tempreture= null;
		Watering watering = null;
		Lighting lighting = null;
		GrowingTips growingTips= null;
		AveLenFlower aveLenFlower = null;
		VisualParameters visualParameters = null;
		String vps = null;
		String vpl = null;
		Flower flower= null;
		String fn= null;
		String fo= null;
		String fs= null;
		String fmp= null;


		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case "tempreture": {
						String meashure = startElement.getAttributeByName(new QName("measure")).getValue();
						nextEvent = reader.nextEvent();
						String content = nextEvent.asCharacters().getData();
						tempreture = new Tempreture(meashure, content);
//						System.out.println(tempreture);
						break;
					}

					case "watering": {
						String meashure = startElement.getAttributeByName(new QName("measure")).getValue();
						nextEvent = reader.nextEvent();
						String content = nextEvent.asCharacters().getData();
						watering = new Watering(meashure, content);
//						System.out.println(watering);
						break;
					}

					case "aveLenFlower": {
						String meashure = startElement.getAttributeByName(new QName("measure")).getValue();
						nextEvent = reader.nextEvent();
						String content = nextEvent.asCharacters().getData();
						aveLenFlower = new AveLenFlower(meashure, content);
//						System.out.println(aveLenFlower);
						break;
					}

					case "lighting": {
						String meashure = startElement.getAttributeByName(new QName("lightRequiring")).getValue();
						lighting = new Lighting(meashure);
//						System.out.println(lighting);
						break;
					}

					case "stemColour": {
						nextEvent = reader.nextEvent();
						vps = nextEvent.asCharacters().getData();
						break;
					}

					case "leafColour": {
						nextEvent = reader.nextEvent();
						vpl = nextEvent.asCharacters().getData();
						break;
					}
					case "name": {
						nextEvent = reader.nextEvent();
						fn = nextEvent.asCharacters().getData();
						break;
					}
					case "origin": {
						nextEvent = reader.nextEvent();
						fo = nextEvent.asCharacters().getData();
						break;
					}

					case "soil": {
						nextEvent = reader.nextEvent();
						fs = nextEvent.asCharacters().getData();
						break;
					}

					case "multiplying": {
						nextEvent = reader.nextEvent();
						fmp = nextEvent.asCharacters().getData();
						break;
					}

				}

			}

			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				switch (endElement.getName().getLocalPart()){
					case "visualParameters":{
						visualParameters = new VisualParameters(aveLenFlower, vps, vpl);
//						System.out.println(visualParameters);
						break;
					}

					case "growingTips":{
						growingTips = new GrowingTips(tempreture, watering, lighting);
//						System.out.println(growingTips);
						break;
					}

					case "flower":{
						flower = new Flower(fn,fo,fs,visualParameters,growingTips,fmp);
						fls.setFlower(flower);
						break;
					}
				}
			}
		}

//		fls.getFlower().forEach(System.out::println);

	}



}